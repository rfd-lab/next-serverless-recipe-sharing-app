# Serverless recipe sharing app built with Next.js and GraphQL

## Frontend Serverless with React and GraphQL

A serverless recipe sharing application in React using Next.js. This is the code that we build in the [Frontend Serverless with React and GraphQL]


### How to use this Project

If you just want to run the app, check out the latest on the `master` branch and then create a `.env` file in the root of your file. It should have the following variables:

_.env_

```
GRAPHCMSID=your-graphcms-id
GRAPHCMSURL=https://api-yourregion.graphcms.com/v1
APIURL=https://www.filestackapi.com/api/store/S3
APIKEY=your-graphcms-api-key
PROJECTID=your-graphcms-projectid
BRANCH=master
CDNBASE=https://cdn.filestackcontent.com/
domain=your-domain.auth0.com
clientId=your-auth0-clientid
clientSecret=your-auth0-clientSecret
scope='openid profile'
redirectUri=https://localhost:3000/api/callback
postLogoutRedirectUri=https://localhost:3000/
cookieSecret='one-two-three-secret-four-secret-dont-share-it-ever'
BACKEND_URL=https://localhost:3000/api/graphql
GRAPHCMSTOKEN=your.graphcms.token
```

You can get these variables by creating an account with GraphCMS and Auth0 and we cover where to get those from and how they get loaded into this project by Next.js in steps 10 and 19, respectively.

You can start the app locally by running `npm start`.

To serverlessly deploy make sure that you have `now` and `dotenv-cli` installed globally and run `npm run secrets:add` followed by `now` in your command line. Be sure to update any of your urls from `localhost:3000` to the address of your actual website.

### Benefits

- Auth0 for User Authentication and Authorization
- Expressive data fetching with Apollo Hooks
- SEO Optimized with Next.js
- Beautiful GraphCMS backend
- Automatic code pipeline and deployment with Zeit Now

